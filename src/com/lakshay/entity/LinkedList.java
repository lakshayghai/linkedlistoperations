package com.lakshay.entity;

/**
 * 
 * @author Lakshay
 *
 */
public class LinkedList {
	
	private LinkedListNode head;
	
	public LinkedList() {

	}

	public LinkedList(LinkedListNode head) {
		this.head = head;
	}



	public void push(int newData) {
		LinkedListNode newNode = new LinkedListNode(newData);
        newNode.next = head;
        head = newNode;
    }
	
	/**
	 * @return the head
	 */
	public LinkedListNode getHead() {
		return head;
	}

	/**
	 * @param head the head to set
	 */
	public void setHead(LinkedListNode head) {
		this.head = head;
	}



	public static LinkedListNode reverse(LinkedListNode list) {
		LinkedListNode prev = null;
		LinkedListNode current = list;
		LinkedListNode next;
        while (current != null) {
            next = current.next;
    	    current.next = prev;
    	    prev = current;
    	    current = next;
    	}
        list = prev;
        return list;
    }
	
	public static void printList(LinkedListNode list) {
		while(list !=null) {
			System.out.print(list.data);
			if(list.next != null){
				System.out.print("->");
			}
			list = list.next;
		}
	}
	
	public static LinkedList sumLinkedList(LinkedListNode list1, LinkedListNode list2) {
		LinkedList result = new LinkedList();
		LinkedListNode list1Reverse = LinkedList.reverse(list1);
		LinkedListNode list2Reverse = LinkedList.reverse(list2);
		int carry = 0;
		while(list1Reverse != null && list2Reverse != null){
			int sum = 0;
			sum = (list1Reverse.data + list2Reverse.data + carry)%10;
			carry = (list1Reverse.data + list2Reverse.data + carry)/10;
			result.push(sum);
			list1Reverse = list1Reverse.next;
			list2Reverse = list2Reverse.next;
		}
		if(list1Reverse != null) {
			while(list1Reverse != null) {
				result.push((list1Reverse.data + carry)%10);
				carry = (list1Reverse.data + carry)/10;
				list1Reverse = list1Reverse.next;
			}
		}
		if(list2Reverse != null) {
			while(list2Reverse != null) {
				result.push((list2Reverse.data + carry)%10);
				carry = (list2Reverse.data + carry)/10;
				list2Reverse = list2Reverse.next;
			}
		}
		if(carry > 0) {
			result.push(carry);
		}

		return result;
	}
	
	

}
