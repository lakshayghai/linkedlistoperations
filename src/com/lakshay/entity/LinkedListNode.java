package com.lakshay.entity;
/**
 * 
 * @author Lakshay
 *
 */

class LinkedListNode {
    int data;
    LinkedListNode next;
         
    LinkedListNode(int d) {
        data = d;
        next = null;
    }
}
