package com.lakshay;

import com.lakshay.entity.LinkedList;

public class Main {

    public static void main(String[] args) {
	    LinkedList llist1 = new LinkedList();
	        llist1.push(5);
	        llist1.push(6);
	        llist1.push(7);
	        llist1.push(1);
	        llist1.push(9);
	    LinkedList llist2 = new LinkedList();
	        llist2.push(6);
	        llist2.push(3);
	        llist2.push(5);
	        llist2.push(9);
	        llist2.push(1);
	        llist2.push(9);
	    LinkedList.printList(llist1.getHead());
	    System.out.println("");
	    LinkedList.printList(llist2.getHead());
	    
	    LinkedList result = LinkedList.sumLinkedList(llist1.getHead(), llist2.getHead());
	    System.out.println("");
	    LinkedList.printList(result.getHead());

	}

}
